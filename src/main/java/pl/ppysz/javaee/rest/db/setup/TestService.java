package pl.ppysz.javaee.rest.db.setup;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.ppysz.javaee.rest.dto.User;

@Stateless
public class TestService {

    @PersistenceContext
    EntityManager em;

    public List<User> getUsers() {
        return em.createQuery("select o from User o", User.class).getResultList();
    }

}
