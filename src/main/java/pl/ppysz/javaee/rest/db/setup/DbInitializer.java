package pl.ppysz.javaee.rest.db.setup;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.ppysz.javaee.rest.dto.User;

@Startup
@Singleton
public class DbInitializer {

    @PersistenceContext
    EntityManager em;

    @PostConstruct
    public void init() {
        em.persist(new User(1, "John", "Constantine"));
        em.persist(new User(2, "Bruce", "Wayne"));
    }
}
