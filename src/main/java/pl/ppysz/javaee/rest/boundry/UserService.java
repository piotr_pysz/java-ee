package pl.ppysz.javaee.rest.boundry;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pl.ppysz.javaee.rest.dao.UserDao;
import pl.ppysz.javaee.rest.db.setup.TestService;
import pl.ppysz.javaee.rest.dto.User;

@Path("/users")
public class UserService {

    UserDao userDao = new UserDao();
    @Inject
    TestService testService;

    @GET
    @Path("display")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getUsers() {
        return testService.getUsers();
    }

    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public User createUser(@FormParam("id") int id,//
            @FormParam("name") String name,//
            @FormParam("surname") String surname) {
        User user = new User(id, name, surname);
        // userDao.addUser(user);
        return user;
    }

}
